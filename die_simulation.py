import random as r
import math as m

DIE_SIZE = 6
DIE_AMOUNT = 5
THROW_SIZE = 10000
THROW_AMOUNT = int(input("How many batches? "))


total_tri = 0
total_roll = 0
total_double = 0
total_quad = 0
total_quint = 0

def round_down(n, decimals=0):
    multiplier = 10**decimals
    return m.floor(n * multiplier) / multiplier


while total_roll < THROW_SIZE * THROW_AMOUNT:
	for i in range(THROW_SIZE):
		die = []
		for i in range(DIE_AMOUNT):
			die.append(r.randint(1,DIE_SIZE))

		if die[0] == die[1] and die[1] == die[2] and die[2] == die[3] and die[3] == die[4]:
			total_quint += 1


		t_d = 0
		t_t = 0
		t_q = 0
		for i in range(len(die)):
			for n in range(len(die)):
				if i == n:
					break
				elif die[i-1] == die[n-1]:
					t_d = 1
				else: 
					break
				
				for x in range(len(die)):
					if i == x or n == x:
						break
					elif die[i-1] == die[x-1]:
						t_t = 1
					else:
						break
					
					for y in range(len(die)):
						if i == y or n == y or x == y:
							break
						elif die[i-1] == die[y-1]:
							t_q = 1
						else:
							break
		if t_d:
			total_double += 1
		if t_t:
			total_tri += 1
		if t_q:
			total_quad += 1
	

	
	total_roll += THROW_SIZE
	print(f"{total_roll/THROW_SIZE} / {THROW_AMOUNT}")

prc_tri = round_down(total_tri/total_roll * 100, 5)
prc_double = round_down(total_double/total_roll * 100, 5)
prc_quad = round_down(total_quad/total_roll * 100, 5)
prc_quint = round_down(total_quint/total_roll * 100, 5)

print("Simulation completed. Here are the results:")
print(f"You got {total_double} doubles, which is ~{prc_double}% of all rolls.")
print(f"You got {total_tri} triples, which is ~{prc_tri}% of all rolls.")
print(f"You got {total_quad} quadrouples, which is ~{prc_quad}% of all rolls.")
print(f"You got {total_quint} quintouples, which is ~{prc_quint}% of all rolls.")

