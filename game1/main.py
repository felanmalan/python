import pygame
from pygame.locals import *

import random as r

NAME = "Bullet Hell"


class Object(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height, game):
        super().__init__()
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.game = game
        self.color = "grey"

        self.rect = Rect(self.x, self.y, self.width, self.height)

        self.setup()

    def draw(self):
        pygame.draw.rect(game.screen, self.color, self.rect)

    def setup():
        pass


class Bird(Object):
    def setup(self):
        self.color = "white"
        self.speed = 2
        self.velocity = [0,0]

        self.life = 3

        self.fly = 1
    
    def move(self):
        self.velocity = [0,0]
        
        if self.game.player.rect.center[0] > self.rect.center[0] and self.fly:
            self.fly = 0
            self.rect = Rect(self.rect.center[0], self.rect.center[1], self.height, self.width)
        
        if self.fly:
            self.velocity[0] -= self.speed * 3
        else:
            self.velocity[1] += self.speed * 5
        
        
        self.rect.move_ip(self.velocity)

        if self.rect.center[0] > self.game.SCREEN_WIDTH + self.width:
            self.kill()
        elif self.rect.center[1] > self.game.SCREEN_HEIGHT + self.height:
            self.kill()
        

class Bullet(Object):
    def set_speed(self, speed_x, speed_y):
        self.speed = [speed_x * self.game.SCREEN_HEIGHT/200, speed_y * self.game.SCREEN_HEIGHT/200]
    
    def set_color(self, color):
        self.color = color

    def setup(self):
        self.color = "red"
        self.speed = [0,0]
        for object in self.game.objects:
            if object == self: break
            elif self.rect.colliderect(object):
                self.rect.y = r.randint(0,self.game.SCREEN_HEIGHT)

    def move(self):
        self.rect.move_ip(self.speed)
        if self.rect.center[0]  > self.game.SCREEN_WIDTH + self.width:
            self.kill()
        elif self.rect.center[1] > self.game.SCREEN_HEIGHT + self.height:
            self.kill()


class Player(Object):
    def setup(self):
        self.surface = pygame.Surface((self.width, self.height))
        self.color = "green"
        self.acceleration = self.game.SCREEN_HEIGHT/300
        self.deacceleration = 1.5
        self.max_speed = self.game.SCREEN_HEIGHT/150

        self.velocity = [0,0]

    def move(self):
        key = pygame.key.get_pressed()

        def input():
            if key[pygame.K_LEFT]: v[0] += -1
            if key[pygame.K_RIGHT]: v[0] += 1
            if key[pygame.K_UP]: v[1] += -1
            if key[pygame.K_DOWN]: v[1] += 1
        def acceleration():
            if self.velocity[0] > self.max_speed:
                self.velocity[0] = self.max_speed
            elif self.velocity[0] < -self.max_speed:
                self.velocity[0] = -self.max_speed
            elif v[0] == 0: self.velocity[0] /= self.deacceleration

            if self.velocity[1] > self.max_speed:
                self.velocity[1] = self.max_speed
            elif self.velocity[1] < -self.max_speed:
                self.velocity[1] = -self.max_speed
            elif v[1] == 0: self.velocity[1] /= self.deacceleration
        
        v = [0,0]
        input()

        if abs(v[0]) + abs(v[1]) > 1:
            self.velocity[0] += v[0]*self.acceleration*0.7
            self.velocity[1] += v[1]*self.acceleration*0.7
        else:
            self.velocity[0] += v[0]*self.acceleration
            self.velocity[1] += v[1]*self.acceleration

        acceleration()

        self.rect.move_ip(self.velocity)


class Game:
    def __init__(self, s_width, s_height, fps):
        self.SCREEN_WIDTH = s_width
        self.SCREEN_HEIGHT = s_height
        self.FPS = fps

        self.objects = []
        self.enemies = []

        pygame.init()
        self.screen = pygame.display.set_mode((self.SCREEN_WIDTH,self.SCREEN_HEIGHT))
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font('font/Pixeltype.ttf', 50)
        pygame.display.set_caption(NAME)

        self.player = Player(self.SCREEN_WIDTH/2, self.SCREEN_HEIGHT/2, self.SCREEN_HEIGHT/40, self.SCREEN_HEIGHT/70, self)
        self.objects.append(self.player)

        self.t = 0
        self.run = False
        self.score = 0

        self.car = 0
        self.rain = 0
        self.bird = 0

    def draw(self):
        self.screen.fill("black")
        for object in self.objects:
                object.draw()
    
    def events(self):
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    raise SystemExit
        
        self.movement()
        self.spawning()
        self.collision()

    def collision(self):
        for enemy in self.enemies:
            if self.player.rect.colliderect(enemy):
                self.run = False
                self.__init__(self.SCREEN_WIDTH, self.SCREEN_HEIGHT, self.FPS)
        
        if self.player.rect.left < 0:
            self.run = False
            self.__init__(self.SCREEN_WIDTH, self.SCREEN_HEIGHT, self.FPS)
        elif self.player.rect.right > self.SCREEN_WIDTH:
            self.run = False
            self.__init__(self.SCREEN_WIDTH, self.SCREEN_HEIGHT, self.FPS)
        elif self.player.rect.top < 0:
            self.run = False
            self.__init__(self.SCREEN_WIDTH, self.SCREEN_HEIGHT, self.FPS)
        elif self.player.rect.bottom > self.SCREEN_HEIGHT:
            self.run = False
            self.__init__(self.SCREEN_WIDTH, self.SCREEN_HEIGHT, self.FPS)

    def make_car(self):
        si = r.randint(25,33)
        enemy = Bullet(r.randint(-200,-20), r.randint(0,self.SCREEN_HEIGHT), self.SCREEN_HEIGHT/si, self.SCREEN_HEIGHT/si*0.6, self)
        enemy.set_speed(self.s,0)
        enemy.set_color((r.randint(155,255), r.randint(75,150), r.randint(0,75)))
        self.objects.append(enemy)
        self.enemies.append(enemy)

    def make_rain(self):
        enemy = Bullet(r.randint(0,self.SCREEN_WIDTH), r.randint(-150,-50), self.SCREEN_HEIGHT/130, self.SCREEN_HEIGHT/130, self)
        enemy.set_speed(0,self.s)
        enemy.set_color((r.randint(0,100),r.randint(75,175),255))
        self.objects.append(enemy)
        self.enemies.append(enemy)

    def make_bird(self):
        enemy = Bird(self.SCREEN_WIDTH+100, r.randint(0,self.SCREEN_HEIGHT/2), self.SCREEN_HEIGHT/50, self.SCREEN_HEIGHT/70, self)
        enemy.color = (r.randint(220,255), r.randint(220,255), r.randint(220,255))
        self.objects.append(enemy)
        self.enemies.append(enemy)
    
    def intro(self):
        if self.car >= 2:
            self.car = r.randint(-1,-0)
            for i in range(r.randint(4,5)):
                self.make_car()
        else:
            self.car += 1

        if self.score > 15:
            if self.rain >= 1:
                self.rain = 0
                for i in range(r.randint(2,3)):
                    self.make_rain()
            else:
                self.rain += 1
            
        if self.score > 20:
            if self.bird >= 8:
                self.bird = r.randint(-5,0)
                self.make_bird()
            else:
                self.bird += 1
        
    def e_storm(self):
        self.s = 1+self.score/500
        if self.car >= 2:
            self.car = r.randint(-15,-13)
            for i in range(r.randint(1,3)):
                self.make_car()
        else:
            self.car += 1

        for i in range(r.randint(4,5)):
            self.make_rain()
        
        if self.bird >= 8:
            self.bird = r.randint(-23,-20)
            self.make_bird()
        else:
            self.bird += 1
    
    def drizzle(self):
        if self.car >= 2:
            self.car = r.randint(-2,-1)
            for i in range(r.randint(4,5)):
                self.make_car()
        else:
            self.car += 1

        if self.rain >= 1:
            self.rain = 0
            for i in range(r.randint(4,5)):
                self.make_rain()
        else:
            self.rain += 1
        
        if self.bird >= 8:
            self.bird = r.randint(-12,-11)
            self.make_bird()
        else:
            self.bird += 1


    def spawning(self):
        self.s = 1+self.score/500

        if self.t >= 15:
            if self.score <= 45: self.intro()
            elif self.score <= 55: self.e_storm()
            elif self.score <= 70: self.drizzle()
            else: self.drizzle()

            self.t = 0
        else:
            self.t += 1
        

    def movement(self):
        for object in self.objects:
            if object.move():
                object.move()

    def game(self):
        while self.run:
            self.events()
            self.draw()

            self.score += 1/60

            pygame.display.flip()
            self.clock.tick(self.FPS)

    def loop(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    raise SystemExit
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_UP:
                    self.run = True
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                    self.run = True
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                    self.run = True
                elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                    self.run = True


            message = "Move to start. Move with arrow keys."
            intro_message = self.font.render(message, False, "white")
            intro_rect = intro_message.get_rect(center=(self.SCREEN_WIDTH/2, self.SCREEN_HEIGHT/2))
            self.screen.blit(intro_message, intro_rect)
            

            self.game()
            pygame.display.flip()
            self.clock.tick(10)
        



# Game loop
game = Game(1600, 900, 60)
game.loop()
